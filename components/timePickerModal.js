import React, { Component } from 'react';
import {View } from 'react-native';
import DateTimePicker from "react-native-modal-datetime-picker";

const TimePickerModal = (props) => {
    return (
        <View>
            <DateTimePicker 
                isVisible={props.isDateTimePickerVisible}
                onConfirm={props.handleTimePicked}
                onCancel={props.hideDateTimePicker}
                mode="time"
                is24Hour={true}
                timePickerModeAndroid="spinner" />
        </View>
    )
}

export default TimePickerModal