import React, { Component } from "react";
import { View, Text, StyleSheet } from "react-native";
import Icon from "react-native-vector-icons/Feather";
import Toast from 'react-native-root-toast';

const RenderListItems = (props) => {
  const showDDay = () => {
    const ONE_DAY_MILISEC = 86400000
    const ONE_HOUR_MILISEC = 3600000
    const today = Date.now()
    const date = props.item.date
    const calulateD_day = Math.floor((date - today) / ONE_DAY_MILISEC)
    const calulateRestTime = Math.floor(((date - today) % ONE_DAY_MILISEC) / ONE_HOUR_MILISEC)
    const msg = calulateD_day > 0 ?
      'Rest of Time : - ' + calulateD_day.toString() + 'Day(s) and ' + calulateRestTime + ' hour(s)' :
      'Rest of Time : + ' + calulateD_day.toString() + 'Day(s) and ' + calulateRestTime + ' hour(s)'
    const toastMsg = Toast.show(msg , {
      duration: 3500,
      position: -200,
      backgroundColor: 'white',
      shadowColor: '#a8a8a8',
      textColor: '#282828'
    })
  }

  const resettingTimer = () => {
    props.updateState({
      isResettingTimer: true,
      inputText: props.item.text
    })
    if (props.item.isTimer) {
      props.openTimePicker()
    } else if (props.item.isDate) {
      props.openCalendar()
    }
    
  }

  const getCategoryColor = (category) => {
    switch (category) {
      case 'habitList' : {
        return '#fff5ba'
      }
      case 'important' : {
        return '#ffabab'
      }
      case 'worthlessLimit' : {
        return '#bffcc6'
      }
      case 'worthless' : {
        return '#6eb5ff'
      }
    }
  }

  return (
    <View style={props.item.isExpired ? 
        {...styles.view_style, ...styles.expiredItem} : 
        {...styles.view_style}} >
      
      <View backgroundColor={getCategoryColor(props.item.category)} style={{width:10, height:50}}></View>
      {props.item.priority > 0 ?
        <Icon.Button
        name="lock"
        size={12}
        backgroundColor="transparent"
        color="red"
        style={{marginRight:0}}
        /> : 
        <Icon/> }
      {props.item.isRepeat ? (
        <Icon.Button
          name="repeat"
          size={12}
          backgroundColor="transparent"
          color="#c8c8c8"
          style={{marginRight:0}}
        />
      ) : (
        <Icon />
      )}
      {props.item.isTimer ? (
        <Icon.Button
          name="clock"
          size={12}
          backgroundColor="transparent"
          color={props.item.isExpired ? "red" : "#c8c8c8"}
          style={{marginRight:0}}
        />
      ) : (
        <Icon/>
      )}
      {props.item.isExpired ? (
        <Icon.Button
          name="play"
          size={12}
          backgroundColor="transparent"
          color="black"
          onPress={resettingTimer}
          style={{marginRight:0}} />
      ) : (
        <Icon />
      )}
      {props.item.isDate ? (
        <Icon.Button
          name="calendar"
          size={12}
          backgroundColor="transparent"
          color="#c8c8c8"
          onPress={showDDay}
          style={{marginRight:0}}
        />
      ) : (
        <Icon />
      )}
    
      <Text
        numberOfLines={2}
        style={
          props.item.isDone === 1 ? styles.doneItem : { textDecorationLine: "none", maxHeight:50, paddingLeft:10, flex:1 }
        }
      >
        {props.item.text}
        <Text style={{color:'red'}}>
          {props.item.isExpired ? ' (expired)' : ''}
        </Text>
        
      </Text>
      
    </View>
  );
}
export default RenderListItems

const styles = StyleSheet.create({
  view_style: {
    borderBottomColor: "rgba(154,154,154,0.3)",
    borderBottomWidth: 1,
    justifyContent: "flex-start",
    // maxHeight: 50,
    backgroundColor: "#fff",
    flexDirection:'row',
    flex:1,
    alignItems:'center'
  },
  expiredItem: {
    backgroundColor: 'rgb(255, 211, 211)'
  },
  doneItem: {
    maxHeight:50, 
    paddingLeft:10, 
    flex:1,
    textDecorationLine: "line-through"
  }
});
