import React, { Component } from 'react';
import { StyleSheet, View, InputAccessoryView } from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';

const InputAccessories = (props) => {
  return (
    <InputAccessoryView nativeID="uniqeID" style={{flexDirection:'row', flex:1}}>
      <View style={styles.inputaccessory_right}>
        <Icon.Button
          onPress={props.openCalendar}
          name="ios-repeat"
          backgroundColor="transparent"
          color={props.isSetRepeat ? 'red' : 'black'}
          underlayColor="white"
          style={styles.btnActive}
          size={20}
          iconStyle={{marginHorizontal:2}}
        />
        <Icon.Button
          onPress={props.openTimePicker}
          name="ios-timer"
          backgroundColor="transparent"
          color={props.isSetTimer ? 'red' : 'black'}
          underlayColor="white"
          style={styles.btnActive}
          size={20}
          iconStyle={{marginHorizontal:2}}
        />
      </View>
    </InputAccessoryView>
  )
}
export default InputAccessories

const styles = StyleSheet.create({
    inputaccessory_left: {
        color:'white', 
        fontSize:20, 
        flex:1,
        flexDirection:'row',
        justifyContent: 'flex-start',
        backgroundColor: '#F4F4F4a3'
      },
      inputaccessory_right: {
        color:'white', 
        fontSize:20, 
        flex:1,
        flexDirection:'row',
        justifyContent: 'flex-end',
        backgroundColor: '#F4F4F4a3'
      },
      btnActive: {
        alignItems:'center',
        opacity: 1
      },
      btnNotActive: {
        alignItems:'center',
        opacity: 0.3
      },
})