import ActionButton from "react-native-circular-action-menu";
import React, { Component } from "react";
import { View, StyleSheet } from "react-native";
import Icon from "react-native-vector-icons/Feather";

export default class FloatingBtn extends Component {
  constructor(props) {
    super(props);
  }

  gotoAchivement = () => {
    this.props.navigate('Achivement')
  }
  gotoSetting = () => {
    this.props.navigate('Setting')
  }
  render() {
    return (
      
        <ActionButton buttonColor="rgba(231,76,60,1)" backdrop={true} onPress={() => alert('asdf')}>
          <ActionButton.Item
            buttonColor="#9b59b6"
            title="New Task"
            onPress={this.gotoSetting}
          >
            <Icon name="settings" style={styles.actionButtonIcon} />
          </ActionButton.Item>

          <ActionButton.Item
            buttonColor="#3498db"
            title="Notifications"
            onPress={this.gotoAchivement}
          >
            <Icon name="activity" style={styles.actionButtonIcon} />
          </ActionButton.Item>

          <ActionButton.Item
            buttonColor="red"
            title="Notifications"
            onPress={this.props.deleteAll.bind(this)}
          >
            <Icon name="trash" style={styles.actionButtonIcon} />
          </ActionButton.Item>

          <ActionButton.Item>
            <Icon />
          </ActionButton.Item>
          <ActionButton.Item>
            <Icon />
          </ActionButton.Item>
        </ActionButton>
     
    );
  }
}

const styles = StyleSheet.create({
  actionButtonIcon: {
    fontSize: 15,
    height: 22,
    color: "white"
  }
});
