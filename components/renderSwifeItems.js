import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native';
import Icon from 'react-native-vector-icons/Feather';

const RenderSwifeItems = (props) => {

    return (
        <View style={styles.rowBack}>
            <View style={styles.row_left}>
                <TouchableOpacity 
                    style={{...styles.row_item, backgroundColor:'red'}} 
                    onPress={props.deleteRow.bind(this, props.data)} >
                    <Icon name="trash" color="white" size={15}></Icon>
                </TouchableOpacity>
            </View>
            <View style={styles.row_right}>
                {props.data.item.priority > 0 ?
                <TouchableOpacity 
                    style={{...styles.row_item, }}
                    onPress={props.togglePinRow.bind(this, props.data, true)} >
                    <Icon name="unlock" color="white" size={15}></Icon>
                </TouchableOpacity> :
                <TouchableOpacity 
                    style={{...styles.row_item, }}
                    onPress={props.togglePinRow.bind(this, props.data, false)} >
                    <Icon name="lock" color="white" size={15}></Icon>
                </TouchableOpacity>}
                {props.data.item.isDone ? 
                <TouchableOpacity 
                    style={{...styles.row_item, backgroundColor:'green'}} 
                    onPress={props.toggleCompleteRow.bind(this, props.data)} >
                    <Icon name="rotate-cw" color="white" size={15}></Icon>
                </TouchableOpacity> :
                <TouchableOpacity 
                    style={{...styles.row_item, backgroundColor:'green'}} 
                    onPress={props.toggleCompleteRow.bind(this, props.data)} >
                    <Icon name="check" color="white" size={15}></Icon>
                </TouchableOpacity> }
                
            </View>
        </View>
    )
}

export default RenderSwifeItems

const styles = StyleSheet.create({
    rowBack: {
		alignItems: 'center',
		backgroundColor: '#DDD',
		flex: 1,
		flexDirection: 'row',
		justifyContent: 'space-between'
    },
    row_left: {
        flex:1, 
        height:50, 
        flexDirection:'row', 
        justifyContent:'flex-start', 
        alignContent:'center'
    },
    row_right: {
        flex:1, 
        height:50, 
        flexDirection:'row', 
        justifyContent:'flex-end', 
        alignContent:'center'
    },
    row_item: {
        height:50, width:50, 
        justifyContent:'center', 
        alignItems:'center'
    }
})