import React, { Component } from 'react';
import {View} from 'react-native';
import DateTimePicker from "react-native-modal-datetime-picker";

const CalendarModal = (props) => {
    return (
        <View>
            <DateTimePicker 
                isVisible={props.toggleCalendar}
                onConfirm={props.updateDatePicker}
                onCancel={props.closeModalPr}
                mode="date"
                is24Hour={true}
                timePickerModeAndroid="spinner" />
        </View>
    )
}
export default CalendarModal