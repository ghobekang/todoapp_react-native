
import {createStackNavigator, createAppContainer} from 'react-navigation';
import AchivementPage from './pages/achivement_page';
import SettingPage from './pages/setting_page';
import HomeScreen_android from './pages/android_home';

const MainNavigator = createStackNavigator({
  Home: {screen: HomeScreen_android},
  Achivement: {screen: AchivementPage},
  Setting: {screen: SettingPage},
}, {
  defaultNavigationOptions: {
    headerTitle: "Focus on Important",
    headerStyle: {
      backgroundColor: '#FEFBF3',
    },
    headerTintColor: '#807E7A',
    headerTitleStyle: {
      fontWeight: 'bold',
      fontSize: 16
    },
    
  },
  initialRouteName: "Home"
});

const App = createAppContainer(MainNavigator);

export default App;
