import React, { Component } from "react";
import {
  View,
  TextInput,
  TouchableOpacity,
  Alert,
  StyleSheet
} from "react-native";
import AsyncStorage from "@react-native-community/async-storage";
import Icon from "react-native-vector-icons/Feather";
import CalendarModal from "../components/calendar_modal";
import { TabView, SceneMap } from "react-native-tab-view";
import Animated from "react-native-reanimated";
import RenderListItems from "../components/renderListItems";
import TimePickerModal from "../components/timePickerModal";
import { SwipeListView } from "react-native-swipe-list-view";
import RenderSwifeItems from "../components/renderSwifeItems";
import * as _ from "lodash";
import BackgroundTimer from "../components/backgroundTimer_custom";
import { KeyboardAccessoryView } from "react-native-keyboard-input";
import SplashScreen from "react-native-splash-screen";
import {AdMobBanner} from 'react-native-admob'

export default class HomeScreen_android extends Component {
  constructor(props) {
    super(props);
    this.state = {
      text: "+ Add Schedule",
      inputText: "",
      inputDate: {},
      inputTimer: "",
      activeNum: 0,
      toggleCalendar: false,
      toggleTimePicker: false,
      isSetDate: false,
      isSetRepeat: false,
      isSetTimer: false,
      isImportant: false,
      isResettingTimer: false,
      todoLists: [],
      habitList: [],
      important: [],
      worthlessLimit: [],
      worthless: [],
      index: 0,
      routes: [
        { key: "all", title: "show a whole todo" },
        { key: "red", title: "Important/Time Limited" },
        { key: "yellow", title: "Important/Time UnLimitted(to make a habit)" },
        { key: "green", title: "Worthless/Time Limited" },
        { key: "blue", title: "Worthless at all" }
      ]
    };
  }

  _renderItem = ({ item, index }) => {
    return (
      <View>
        {index % 4 === 0 && index !== 0 ? (
          <View style={{ backgroundColor: "white", alignItems: "center" }}>
            <AdMobBanner
              adUnitID="ca-app-pub-5635953962161470/6791504099"
              adSize="Banner"
              testDevices={[AdMobBanner.simulatorId]}
            />
          </View>
        ) : null}
        <RenderListItems
          index={index}
          item={item}
          updateState={this.setState.bind(this)}
          openTimePicker={this.openTimePicker}
          isResettingTimer={this.state.isResettingTimer}
          openCalendar={this.openCalendar}
        />
      </View>
    );
  };
  _renderTodoList = listNum => {
    const todoNameList = [
      "todoLists",
      "important",
      "habitList",
      "worthlessLimit",
      "worthless"
    ];

    return (
      <View>
        <SwipeListView
          useFlatList
          data={this.state[todoNameList[listNum]]}
          extraData={this.state[todoNameList[listNum]]}
          keyExtractor={(item, index) => item.timestamp.toString()}
          renderItem={this._renderItem}
          renderHiddenItem={(data, rowMap) => (
            <RenderSwifeItems
              data={data}
              togglePinRow={this.togglePinRow}
              deleteRow={this.deleteRow}
              toggleCompleteRow={this.toggleCompleteRow}
            />
          )}
          leftOpenValue={50}
          rightOpenValue={-100}
          contentContainerStyle={{ flexShrink: 1 }}
        />
      </View>
    );
  };

  deleteRow = data => {
    const lists = this.getCategoryList(data.item.category);
    const allLists = this.state.todoLists;

    _.pullAt(lists, data.index);
    _.pull(allLists, _.find(allLists, { text: data.item.text }));

    this.setState({ allLists });
  };

  deleteAll = () => {
    const confirmReq = new Promise((resolve, reject) => {
      Alert.alert(
        "confirm",
        "Are you sure to delete All Item?",
        [
          {
            text: "No",
            onPress: () => resolve(false),
            style: "cancel"
          },
          { text: "Yes", onPress: () => resolve(true) }
        ],
        { cancelable: false }
      );
    });
    confirmReq.then(result => {
      if (result) {
        this.setState(state => {
          return {
            todoLists: [],
            habitList: [],
            important: [],
            worthlessLimit: [],
            worthless: []
          };
        });
      }
    });
  };

  togglePinRow = (data, isLocked) => {
    const targetItem = data.item;
    const categoryList = this.getCategoryList(data.item.category);
    const entireItemList = this.state.todoLists;

    if (isLocked) {
      this.updateValue(categoryList, entireItemList, targetItem, 0, "priority");
    } else {
      this.updateValue(categoryList, entireItemList, targetItem, 1, "priority");
    }
  };

  toggleCompleteRow = data => {
    const targetItem = data.item;
    const categoryList = this.getCategoryList(data.item.category);
    const entireItemList = this.state.todoLists;

    if (data.item.isDone === 1) {
      this.updateValue(categoryList, entireItemList, targetItem, 0, "isDone");
    } else {
      this.updateValue(categoryList, entireItemList, targetItem, 1, "isDone");
    }
  };

  updateValue = (categoryList, entireItemList, targetItem, val, targetName) => {
    if (typeof val === "number") {
      _.chain(categoryList)
        .find(targetItem)
        .update(targetName, n => {
          return val;
        })
        .value();
      _.chain(entireItemList)
        .find(targetItem)
        .update(targetName, n => {
          return val;
        })
        .value();

      if (targetName === "isDone") {
        this.setCategoryList(targetItem.category, categoryList);
        this.setState({ todoLists: entireItemList });
      } else if (targetName === "priority") {
        this.setCategoryList(
          targetItem.category,
          _.orderBy(categoryList, ["priority"], ["desc"])
        );
        this.setState({
          todoLists: _.orderBy(entireItemList, ["priority"], ["desc"])
        });
      }
    }
  };

  getCategoryList = category => {
    return this.state[category];
  };

  setCategoryList = (category, data) => {
    this.setState(state => {
      switch (category) {
        case "habitList": {
          return {
            habitList: data
          };
        }
        case "important": {
          return {
            important: data
          };
        }
        case "worthlessLimit": {
          return {
            worthlessLimit: data
          };
        }
        case "worthless": {
          return {
            worthless: data
          };
        }
      }
    });
  };

  inputEnter_keyEvent = event => {
    if (event.nativeEvent.text.length !== 0) {
      this.addTodoItem();
    }
    this.textInput.clear();
  };

  expireItemTimer = message => {
    const $_ = require("lodash");
    const lists = [
      "todoLists",
      "habitList",
      "important",
      "worthlessLimit",
      "worthless"
    ];
    for (let item of lists) {
      const targetItems = this.state[item];
      const target = _.find(targetItems, { text: message }) || [];

      if (targetItems.length > 0 && target.length !== 0) {
        if (target.isDone === 0) {
          $_.chain(targetItems)
            .find({ text: message, isExpired: false })
            .update("isExpired", n => {
              return true;
            })
            .value();
          this.setState({ todoLists: this.state.todoLists });

          return true;
        }
      }
    }

    return false;
  };

  initInput = () => {
    this.setState({
      isSetRepeat: false,
      isSetDate: false,
      isSetTimer: false,
      isExpired: false,
      inputDate: {},
      inputTimer: "",
      inputText: ""
    });
    this.textInput.clear();

    return false;
  };

  addTodoItem = () => {
    const _self = this;
    const d = new Date();
    const dataset = {
      timestamp: Date.now(),
      text: this.state.inputText,
      date:
        this.state.inputDate ||
        d.getUTCFullYear() +
          "-" +
          (parseInt(d.getUTCMonth()) + 1) +
          "-" +
          d.getUTCDate(),
      isRepeat: this.state.isSetRepeat,
      isTimer: this.state.isSetTimer,
      isDate: this.state.isSetDate,
      isImportant: this.state.isImportant,
      // when you set a Timer, should pick an only one thing up which is important or not
      isDone: 0,
      isExpired: false,
      priority: 0
    };

    const classify = this.getClassify(dataset);
    dataset["category"] = classify;
    this.appendItem(classify, dataset);

    if (this.state.isSetTimer) {
      const FIVE_MINUTES_MILISECONDS = 300000;
      this.setBackgroundTimer(
        FIVE_MINUTES_MILISECONDS + 60000,
        FIVE_MINUTES_MILISECONDS,
        false
      );
    } else if (this.state.isSetDate) {
      const ONEDAY_MILISECONDS = 86400000;
      this.setBackgroundTimer(ONEDAY_MILISECONDS, 0, true);
    }
  };

  setBackgroundTimer = (amountOfTime, expectedTime, isDatePeriod) => {
    const date = isDatePeriod ? this.state.inputDate : this.state.inputTimer;
    const diffTime = date - Date.now();
    const notiTime =
      diffTime - amountOfTime > 0 ? diffTime - expectedTime : diffTime;
    if (isDatePeriod && notiTime < amountOfTime) {
      alert("You should pick a date at least one day later");
      return false;
    }
    const message = this.state.inputText;

    // one time background timer
    BackgroundTimer.runBackgroundTimer(
      () => {
        //push notification or alram
        const result = this.expireItemTimer(message);
        if (result) {
          // ㄱㅖ산이 좀 잘 못되는 것 같은데
          const WILL_DIVIDE_VALUE = isDatePeriod ? 86400000 : 60000
          let REST_OF_TIME = date - Date.now()
          if (REST_OF_TIME < 0 ) {
            REST_OF_TIME = 0
          }

          alert(
            message +
              " 남은시간 : " +
              Math.round(
                REST_OF_TIME / WILL_DIVIDE_VALUE
              ).toString()
          );
        }

        BackgroundTimer.stopBackgroundTimer();

        return false;
      },
      notiTime,
      false
    );
  };

  appendItem = (category, data) => {
    const save = new Promise((resolve, reject) => {
      this.setState(state => {
        const lists = [...state[category], data];
        const accuLists = [...state.todoLists, data];

        switch (category) {
          case "habitList": {
            resolve();
            return {
              habitList: lists,
              todoLists: accuLists,
              index: 2
            };
          }
          case "important": {
            resolve();
            return {
              important: lists,
              todoLists: accuLists,
              index: 1
            };
          }
          case "worthlessLimit": {
            resolve();
            return {
              worthlessLimit: lists,
              todoLists: accuLists,
              index: 3
            };
          }
          case "worthless": {
            resolve();
            return {
              worthless: lists,
              todoLists: accuLists,
              index: 4
            };
          }
        }
      });
    });

    save.then(() => {});
  };

  getClassify = data => {
    if (data.isRepeat) {
      return "habitList";
    } else if ((data.isTimer || data.isDate) && data.isImportant) {
      return "important";
    } else if ((data.isTimer || data.isDate) && !data.isImportant) {
      return "worthlessLimit";
    } else {
      return "worthless";
    }
  };

  openCalendar = () => {
    this.setState({ toggleCalendar: true });
    return false;
  };

  closeModal = () => {
    this.setState({ toggleCalendar: false });
    return false;
  };

  pickCertainDate = date => {
    if (date) {
      const confirm = new Promise(function(resolve, reject) {
        Alert.alert(
          "confirm",
          "Is This Important?",
          [
            {
              text: "No",
              onPress: () => resolve(false),
              style: "cancel"
            },
            { text: "Yes", onPress: () => resolve(true) }
          ],
          { cancelable: false }
        );
      });
      confirm.then(result => {
        if (result) {
          this.setState({
            inputDate: date,
            isSetDate: true,
            isImportant: true
          });
          this.props.isSetRepeat
            ? this.setState({ isSetRepeat: false })
            : this.setState({ isSetRepeat: true });
          this.closeModal();
        }
      });
    }

    return false;
  };

  openTimePicker = () => {
    this.setState({ toggleTimePicker: true });
    return false;
  };

  closeTimePicker = () => {
    this.setState({ toggleTimePicker: false });
    return false;
  };

  handleTimePicked = date => {
    const confirm = new Promise(function(resolve, reject) {
      Alert.alert(
        "confirm",
        "Is This Important?",
        [
          {
            text: "No",
            onPress: () => resolve(false),
            style: "cancel"
          },
          { text: "Yes", onPress: () => resolve(true) }
        ],
        { cancelable: false }
      );
    });

    confirm.then(isImportant => {
      this.setState({
        isSetTimer: true,
        inputTimer: date,
        isImportant: isImportant
      });
      if (this.state.isResettingTimer) {
        this.updateItemWithTimer();
        this.setState({ isResettingTimer: false });
      }
      this.closeTimePicker();
    });

    return false;
  };

  updateItemWithTimer = () => {
    this.addTodoItem();
    return false;
  };

  _renderTabBar = props => {
    return (
      <View style={{ flexDirection: "row", justifyContent: "space-around" }}>
        {props.navigationState.routes.map((route, i) => {
          let color;
          let isCollection = false;

          switch (i) {
            case 0: {
              color = "black";
              isCollection = true;
              break;
            }
            case 1: {
              color = "red";
              break;
            }
            case 2: {
              color = "orange";
              break;
            }
            case 3: {
              color = "green";
              break;
            }
            case 4: {
              color = "blue";
              break;
            }
          }

          return (
            <TouchableOpacity key={i}>
              <Animated.View
                style={
                  this.state.index === i ? styles.activebtn : styles.unactivebtn
                }
              >
                <Icon.Button
                  name={isCollection ? "package" : "minus"}
                  onPress={() => this.setState({ index: i })}
                  backgroundColor="transparent"
                  color={color}
                  size={20}
                />
              </Animated.View>
            </TouchableOpacity>
          );
        })}
      </View>
    );
  };

  // asyncStore methods
  storeData = async (key, data) => {
    try {
      const dataset = JSON.stringify(data || this.state);

      await AsyncStorage.setItem(key, dataset);
    } catch (e) {
      console.log(e);
    }
  };

  getData = async key => {
    try {
      const value = await AsyncStorage.getItem(key);

      if (value !== null) {
        return JSON.parse(value);
      }
    } catch (e) {
      console.log(e);
    }
  };

  clearAll = async () => {
    try {
      await AsyncStorage.clear();
    } catch (e) {
      // clear error
    }

    console.log("Done.");
  };
  //[end] asyncStore Methods

  saveAllData = () => {
    const d = new Date();
    const today = d
      .toLocaleString()
      .split(" ")[0]
      .replace(/-/g, "");
    this.storeData(today, this.state);
  };

  componentDidUpdate(prevProps, prevState) {
    if (this.state.todoLists.length !== prevState.todoLists.length) {
      this.saveAllData();
    }
  }

  componentWillMount() {
    const _self = this;
    const d = new Date();
    const today = d
      .toLocaleString()
      .split(" ")[0]
      .replace(/-/g, "");

    const data = this.getData(today);
    data.then(result => {
      if (result) {
        this.setState({
          todoLists: result.todoLists,
          habitList: result.habitList,
          important: result.important,
          worthlessLimit: result.worthlessLimit,
          worthless: result.worthless
        });
      }
    });

    BackgroundTimer.runBackgroundTimer(
      () => {
        const timeNow = new Date().getHours();
        if (timeNow === 23) {
          _self.dailyInit();
          //push notification to check report
        }
      },
      1800000,
      true
    );

    SplashScreen.hide();
  }

  dailyInit = () => {
    const checkLists = [
      "todoLists",
      "important",
      "habitList",
      "worthlessLimit",
      "worthless"
    ];
    let analizedLists = [];
    let accuData;
    this.getData("accuData").then(result => {
      accuData = result && result.length !== 0 ? result : [];

      for (let item of checkLists) {
        const data = this.analizeData(this.state[item]);
        analizedLists.push(data);
      }
      const accuDataset = {
        date: new Date().toLocaleDateString(),
        data: this.analizeData(this.state.todoLists)
      };
      accuData.push(accuDataset);

      this.storeData(
        "analized_" + new Date().toLocaleDateString().replace(/-/g, ""),
        analizedLists
      );
      this.storeData("accuData", accuData);

      this.setState({
        todoLists: [...this.state.habitList],
        important: [],
        worthlessLimit: [],
        worthless: []
      });
    });
  };

  analizeData = data => {
    const dataset = {
      totalLen: data ? data.length : 0,
      isDoneLen: _.filter(data, { isDone: 1 })
        ? _.filter(data, { isDone: 1 }).length
        : 0,
      failedLen: _.filter(data, { isExpired: true })
        ? _.filter(data, { isExpired: true }).length
        : 0
    };
    const notDoneLen = dataset.totalLen - dataset.isDoneLen;
    dataset["notDoneLen"] = notDoneLen;
    dataset["date"] = new Date().toLocaleDateString();

    return dataset;
  };

  renderItem_BtmNav = () => {
    return (
      <View style={{ flexDirection: "row" }}>
        <View style={styles.actionBtns_left}>
          <TouchableOpacity
            onPress={this.deleteAll}
            style={styles.actionBtns_bottom}
          >
            <Icon name="trash" size={25} color="red" />
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => this.props.navigation.navigate("Achivement")}
            style={styles.actionBtns_bottom}
          >
            <Icon name="activity" size={25} />
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => this.props.navigation.navigate("Setting")}
            style={{ ...styles.actionBtns_bottom, opacity: 0.5 }}
            disabled={true}
          >
            <Icon name="settings" size={25} />
          </TouchableOpacity>
        </View>
        <View style={styles.actionBtns_right}>
          <TouchableOpacity
            onPress={this.openCalendar}
            style={styles.actionBtns_bottom}
          >
            <Icon
              name="calendar"
              size={25}
              color={this.state.isSetDate ? "orange" : null}
            />
          </TouchableOpacity>
          <TouchableOpacity
            onPress={this.openTimePicker}
            style={styles.actionBtns_bottom}
          >
            <Icon
              name="clock"
              size={25}
              color={this.state.isSetTimer ? "orange" : null}
            />
          </TouchableOpacity>
        </View>
      </View>
    );
  };

  render() {
    const inputAccessoryViewID = "uniqeID";

    return (
      <View style={styles.container}>
        <View style={{ flex: 1 }}>
          <TextInput
            style={styles.textinput}
            onChangeText={text => this.setState({ inputText: text })}
            onSubmitEditing={this.inputEnter_keyEvent}
            onBlur={this.initInput.bind(this)}
            placeholder={this.state.text}
            autoCapitalize="none"
            inputAccessoryViewID={inputAccessoryViewID}
            autoCorrect={false}
            ref={input => {
              this.textInput = input;
            }}
          />

          <TabView
            navigationState={this.state}
            renderScene={SceneMap({
              all: this._renderTodoList.bind(this, 0),
              red: this._renderTodoList.bind(this, 1),
              yellow: this._renderTodoList.bind(this, 2),
              green: this._renderTodoList.bind(this, 3),
              blue: this._renderTodoList.bind(this, 4)
            })}
            onIndexChange={index => this.setState({ index })}
            renderTabBar={this._renderTabBar}
            swipeEnabled={false}
          />

          <CalendarModal
            toggleCalendar={this.state.toggleCalendar}
            closeModalPr={this.closeModal}
            updateDatePicker={this.pickCertainDate}
          />

          <TimePickerModal
            isDateTimePickerVisible={this.state.toggleTimePicker}
            hideDateTimePicker={this.closeTimePicker}
            closeModalPr={this.closeModal}
            handleTimePicked={this.handleTimePicked}
          />

          <KeyboardAccessoryView
            navigate={this.props.navigation.navigate}
            renderContent={this.renderItem_BtmNav}
          />

          <View style={{ alignItems: "center", marginTop: 10 }}>
            <AdMobBanner
                adUnitID="ca-app-pub-5635953962161470/6791504099"
                adSize="Banner"
                testDevices={[AdMobBanner.simulatorId]}
              />
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    padding: 10,
    flex: 1,
    position: "relative"
  },
  textinput: {
    height: 35,
    borderColor: "rgba(154,154,154,0.5)",
    borderWidth: 1,
    padding: 7,
    color: "rgba(154,154,154,0.7)"
  },
  activebtn: {
    opacity: 1
  },
  unactivebtn: {
    opacity: 0.2
  },
  actionBtns_bottom: {
    marginRight: 20,
    marginLeft: 20
  },
  actionBtns_left: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "flex-start"
  },
  actionBtns_right: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "flex-end"
  }
});
