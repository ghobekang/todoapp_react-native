import React, {Component, useState} from 'react';
import {View, TextInput, TouchableOpacity, Alert, StyleSheet} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import Icon from 'react-native-vector-icons/Feather';
import FloatingBtn from '../components/floatingbtn';
import CalendarModal from '../components/calendar_modal';
import InputAccessories from '../components/input_accessories';
import { TabView, SceneMap, TabBar } from 'react-native-tab-view';
import Animated from 'react-native-reanimated';
import RenderListItems from '../components/renderListItems';
import TimePickerModal from '../components/timePickerModal';
import { SwipeListView } from 'react-native-swipe-list-view';
import RenderSwifeItems from '../components/renderSwifeItems';
import * as _ from 'lodash'
import BackgroundTimer from '../components/backgroundTimer_custom';



export default class HomeScreen_ios extends Component {
  constructor(props) {
    super(props)
    this.state = {
      text: '+ Add Schedule',
      inputText: '',
      inputDate: {},
      inputTimer: '',
      activeNum: 0,
      toggleCalendar: false,
      toggleTimePicker: false,
      isSetDate: false,
      isSetRepeat: false,
      isSetTimer: false,
      isImportant: false,
      isResettingTimer: false,
      todoLists: [],
      habitList: [],
      important: [],
      worthlessLimit: [],
      worthless: [],
      index: 0,
      routes: [
        { key: 'all', title: 'show a whole todo'},
        { key: 'red', title: 'Important/Time Limited' },
        { key: 'yellow', title: 'Important/Time UnLimitted(to make a habit)' },
        { key: 'green', title: 'Worthless/Time Limited' },
        { key: 'blue', title: 'Worthless at all' },
      ],
    }
  }

  _renderItem = ({item}) => {
    return (
      <RenderListItems
        item={item}
        updateState={this.setState.bind(this)}
        openTimePicker={this.openTimePicker}
        isResettingTimer={this.state.isResettingTimer}
        openCalendar={this.openCalendar}
      />
    )
  }
  
  _showAllTodo = () => {
  
    return (
      <View style={{flex:1}}>
        <SwipeListView
          useFlatList
          data={this.state.todoLists}
          extraData={this.state.todoLists}
          keyExtractor={(item, index) => item.timestamp.toString()}
          renderItem={this._renderItem}
          renderHiddenItem={ (data, rowMap) => (
            <RenderSwifeItems 
              data={data}
              togglePinRow={this.togglePinRow}
              deleteRow={this.deleteRow}
              toggleCompleteRow={this.toggleCompleteRow} />
          )}
          leftOpenValue={50}
          rightOpenValue={-100} />
      </View>
    )
  }
  
  _FirstRoute = () => {
  
    return (
      <View style={{flex:1}}>
        <SwipeListView
          useFlatList
          data={this.state.important}
          extraData={this.state.important}
          keyExtractor={(item, index) => item.timestamp.toString()}
          renderItem={this._renderItem}
          renderHiddenItem={ (data, rowMap) => (
            <RenderSwifeItems 
              data={data}
              togglePinRow={this.togglePinRow}
              deleteRow={this.deleteRow}
              toggleCompleteRow={this.toggleCompleteRow} />
          )}
          leftOpenValue={50}
          rightOpenValue={-100} />
      </View>
    )
  }
  _SecondRoute = () => {
    return (
      <View style={{flex:1}}>
        <SwipeListView
          useFlatList
          data={this.state.habitList}
          extraData={this.state.habitList}
          keyExtractor={(item, index) => item.timestamp.toString()}
          renderItem={this._renderItem}
          renderHiddenItem={ (data, rowMap) => (
            <RenderSwifeItems 
              data={data}
              togglePinRow={this.togglePinRow}
              deleteRow={this.deleteRow}
              toggleCompleteRow={this.toggleCompleteRow} />
          )}
          leftOpenValue={50}
          rightOpenValue={-100} />
      </View>
    )
  }
  _ThirdRoute = () => {
    return (
      <View style={{flex:1}}>
        <SwipeListView
          useFlatList
          data={this.state.worthlessLimit}
          extraData={this.state.worthlessLimit}
          keyExtractor={(item, index) => item.timestamp.toString()}
          renderItem={this._renderItem}
          renderHiddenItem={ (data, rowMap) => (
            <RenderSwifeItems 
              data={data}
              togglePinRow={this.togglePinRow}
              deleteRow={this.deleteRow}
              toggleCompleteRow={this.toggleCompleteRow} />
          )}
          leftOpenValue={50}
          rightOpenValue={-100} />
      </View>
    )
  }
  _ForthRoute = () => {
    return (
      <View style={{flex:1}}>
        <SwipeListView
          useFlatList
          data={this.state.worthless}
          extraData={this.state.worthless}
          keyExtractor={(item, index) => item.timestamp.toString()}
          renderItem={this._renderItem}
          renderHiddenItem={ (data, rowMap) => (
            <RenderSwifeItems 
              data={data}
              togglePinRow={this.togglePinRow}
              deleteRow={this.deleteRow}
              toggleCompleteRow={this.toggleCompleteRow} />
          )}
          leftOpenValue={50}
          rightOpenValue={-100} />
      </View>
    )
  }

  deleteRow = (data) => {
    const lists = this.getCategoryList(data.item.category)
    const allLists = this.state.todoLists

    _.pullAt(lists, data.index)
    _.pull(allLists, _.find(allLists, {text: data.item.text}))

    this.setState({allLists})
  }

  deleteAll = () => {
    const confirmReq = new Promise((resolve, reject) => {
      Alert.alert(
        'confirm', 
        'Are you sure to delete All Item?',
        [
          {
            text: 'No',
            onPress: () => resolve(false),
            style: 'cancel',
          },
          {text: 'Yes', onPress: () => resolve(true)},
        ],{cancelable: false})  
    })
    confirmReq.then((result) => {
      if (result) {
        this.setState((state) => {
          return {
            todoLists: [],
            habitList: [],
            important: [],
            worthlessLimit: [],
            worthless: [],
          }
        })
      }
    })
  }

  togglePinRow = (data, isLock) => {
    const lists = this.getCategoryList(data.item.category)
    const allLists = this.state.todoLists

    if (isLock) {
      _.chain(lists).find(data.item).update('priority', (n) => { return 0 }).value()
      _.chain(allLists).find(data.item).update('priority', (n) => { return 0 }).value()

      this.setCategoryList(data.item.category, _.orderBy(lists, ['timestamp'], ['asc']))
      this.setState({todoLists : _.orderBy(allLists, ['timestamp'], ['asc'])})
    } else {
      _.chain(lists).find(data.item).update('priority', (n) => { return 1 }).value()
      _.chain(allLists).find(data.item).update('priority', (n) => { return 1 }).value()

      this.setCategoryList(data.item.category, _.orderBy(lists, ['priority'], ['desc']))
      this.setState({todoLists : _.orderBy(allLists, ['priority'], ['desc'])})
    }

  }

  toggleCompleteRow = (data) => {
    const lists = this.getCategoryList(data.item.category)
    const allLists = this.state.todoLists
    
    if (data.item.isDone === 1) {
      _.chain(lists).find(data.item).update('isDone', (n) => { return 0 }).value()
      _.chain(allLists).find(data.item).update('isDone', (n) => { return 0 }).value()
      
      this.setCategoryList(data.item.category, lists)
      this.setState({todoLists: allLists})
    } else {
      _.chain(lists).find(data.item).update('isDone', (n) => { return 1 }).value()
      _.chain(allLists).find(data.item).update('isDone', (n) => { return 1 }).value()
      
      this.setCategoryList(data.item.category, lists)
      this.setState({todoLists: allLists})
    }
  }
  
  getCategoryList = (category) => {
    return this.state[category]
  }

  setCategoryList = (category, data) => {
    this.setState((state) => {

      switch (category) {
        case 'habitList' : {
          return {
            habitList: data
          }
        }
        case 'important' : {
          return {
            important: data
          }
        }
        case 'worthlessLimit' : {
          return {
            worthlessLimit: data
          }
        }
        case 'worthless' : {
          return {
            worthless: data
          }
        }
      }
    })
  }

  keyPressed = (event) => {

    if (event.nativeEvent.text.length !== 0) {
      this.addTodoItem() 
    }
    this.textInput.clear()
    
  }

  expireItemTimer = (message) => {
    const $_ = require('lodash')
    const lists = ['todoLists', 'habitList', 'important', 'worthlessLimit', 'worthless']
    for (item in lists) {
      const targetItems = this.state[lists[item]]
      const target = _.find(targetItems, {text: message}) || []
      
      if (targetItems.length > 0 && target.length !== 0) {
        if (target.isDone === 0) {
          $_.chain(targetItems)
            .find({text: message, isExpired: false})
            .update('isExpired', (n) => { return true })
            .value()
          this.setState({todoLists: this.state.todoLists})

          return true
        }
      }
    }

    return false
  }

  initInput = () => {
    this.setState({
      isSetRepeat: false,
      isSetDate: false,
      isSetTimer: false,
      isExpired: false,
      inputDate: {},
      inputTimer: '',
      inputText: ''
    })
    this.textInput.clear()

    return false
  }

  addTodoItem = () => {
    const _self = this
    const d = new Date()
    const dataset = {
      timestamp: Date.now(),
      text: this.state.inputText,
      date: this.state.inputDate ||  
        d.getUTCFullYear() + '-' + (parseInt(d.getUTCMonth()) + 1) + '-' + d.getUTCDate(),
      isRepeat: this.state.isSetRepeat,
      isTimer: this.state.isSetTimer,
      isDate: this.state.isSetDate,
      isImportant: this.state.isImportant,
      // when you set a Timer, should pick an only one thing up which is important or not
      isDone: 0,
      isExpired: false,
      priority: 0
    }

    const classify = this.getClassify(dataset)
    dataset['category'] = classify
    this.appendItem(classify, dataset)

    if (this.state.isSetTimer) {
      const date = this.state.inputTimer
      const diffTime = date - Date.now()
      const notiTime = diffTime - 360000 > 0 ? diffTime - 300000 : diffTime
      const message = this.state.inputText

      // one time background timer
     BackgroundTimer.runBackgroundTimer(() => {
        //push notification or alram
        const result = _self.expireItemTimer(message)
        if (result) {
          alert(message + ' 남은시간 : ' + Math.round((date - Date.now())/60000).toString())
        }

        BackgroundTimer.stopBackgroundTimer()
        
        return false
      }, notiTime, false)
    } else if (this.state.isSetDate) {
      const date = this.state.inputDate
      const diffTime = date - Date.now()
      const notiTime = diffTime - 86400000
      if (notiTime < 86400000) {
        alert('You must set a date larger than one day')
        return false
      }
      const message = this.state.inputText

      // one time background timer
      BackgroundTimer.runBackgroundTimer(() => {
        //push notification or alram
        const result = _self.expireItemTimer(message)
        if (result) {
          alert(message + ' 남은시간 : ' + Math.round((date - Date.now())/8640000).toString())
        }
        
        BackgroundTimer.stopBackgroundTimer()
        
        return false
      }, notiTime, false)
    }
     
  }
  appendItem = (category, data) => {
    const save = new Promise((resolve, reject) => {
      this.setState((state) => {
        const lists = [...state[category], data]  
        const accuLists = [...state.todoLists, data]
  
        switch (category) {
          case 'habitList' : {
            resolve()
            return {
              habitList: lists,
              todoLists: accuLists,
              index: 2
            }
          }
          case 'important' : {
            resolve()
            return {
              important: lists,
              todoLists: accuLists,
              index: 1
            }
          }
          case 'worthlessLimit' : {
            resolve()
            return {
              worthlessLimit: lists,
              todoLists: accuLists,
              index: 3
            }
          }
          case 'worthless' : {
            resolve()
            return {
              worthless: lists,
              todoLists: accuLists,
              index: 4
            }
          }
        }
      })  
    })

    save.then(() => {
     
    })
  }
  
  getClassify = (data) => {
    if (data.isRepeat) {
      return 'habitList'
    } else if ((data.isTimer || data.isDate) && data.isImportant) {
      return 'important'
    } else if ((data.isTimer || data.isDate) && !data.isImportant) {
      return 'worthlessLimit'
    } else {
      return 'worthless'
    }
  }

  openCalendar = () => {
    this.setState({toggleCalendar: true})
    return false
  }

  closeModal = () => {
    this.setState({toggleCalendar: false})
    return false
  }

  pickCertainDate = (date) => {
    if (date){
      const confirm = new Promise(function(resolve, reject) {
        Alert.alert(
          'confirm', 
          'Is This Important?',
          [
            {
              text: 'No',
              onPress: () => resolve(false),
              style: 'cancel',
            },
            {text: 'Yes', onPress: () => resolve(true)},
          ],{cancelable: false})  
      })
      confirm.then((result) => {
        if (result) {
          this.setState({inputDate: date, isSetDate: true, isImportant: true})
          this.props.isSetRepeat ? 
                  this.setState({isSetRepeat: false}) : 
                  this.setState({isSetRepeat: true})
          this.closeModal()
        }
      })  
    }
    
    return false
  }

  openTimePicker = () => {
    this.setState({toggleTimePicker: true})
    return false
  }
  
  closeTimePicker = () => {
    this.setState({toggleTimePicker: false})
    return false
  }

  handleTimePicked = (date) => {
    
    const confirm = new Promise(function(resolve, reject) {
      Alert.alert(
        'confirm', 
        'Is This Important?',
        [
          {
            text: 'No',
            onPress: () => resolve(false),
            style: 'cancel',
          },
          {text: 'Yes', onPress: () => resolve(true)},
        ],{cancelable: false})  
    })
    
    confirm.then((isImportant) => {
      this.setState({isSetTimer: true, inputTimer: date, isImportant: isImportant})
      if (this.state.isResettingTimer) {
        this.updateItemWithTimer()
        this.setState({isResettingTimer: false})
      }
      this.closeTimePicker()
    })
  
    return false
  }

  updateItemWithTimer = () => {
    this.addTodoItem()
    return false
  }

  _renderTabBar = props => {
    return (
      <View style={{flexDirection:'row', justifyContent:'space-around' }}>
        {props.navigationState.routes.map((route, i) => {
          let color;
          let isCollection = false

          switch (i) {
            case 0 : {
              color = 'black'
              isCollection = true
              break
            }
            case 1 : {
              color = 'red'
              break
            }
            case 2 : {
              color = 'orange'
              break
            }
            case 3 : {
              color = 'green'
              break
            }
            case 4 : {
              color = 'blue'
              break
            }
          }

          return (
            <TouchableOpacity key={i}>
              <Animated.View style={this.state.index === i ? styles.activebtn : styles.unactivebtn}>
                <Icon.Button 
                  name={isCollection ? 'package' : 'minus'}
                  onPress={() => this.setState({ index: i })} 
                  backgroundColor="transparent" 
                  color={color} 
                  size={20} />
              </Animated.View>
              
            </TouchableOpacity>
          );
        })}
      </View>
    );
  };

  // asyncStore methods
  storeData = async (key, data) => {
    try {
      const dataset = JSON.stringify(data || this.state)

      await AsyncStorage.setItem(key, dataset)
    } catch (e) {
      console.log(e)
    }
  }

  getData = async (key) => {
    try {
      
      const value = await AsyncStorage.getItem(key)
      
      if(value !== null) {
        return JSON.parse(value)
      }
    } catch(e) {
      console.log(e)
    }
  }

  clearAll = async () => {
    try {
      await AsyncStorage.clear()
    } catch(e) {
      // clear error
    }
  
    console.log('Done.')
  }
  //[end] asyncStore Methods

  saveAllData = () => {
    const d = new Date()
    const today = d.toLocaleString().split(' ')[0].replace(/-/g, '')
    this.storeData(today, this.state)
  }

  componentDidUpdate(prevProps, prevState) {
    if (this.state.todoLists.length !== prevState.todoLists.length) {
      this.saveAllData()
    }
  }
 

  componentWillMount() {
    const _self = this
    const d = new Date()
    const today = d.toLocaleString().split(' ')[0].replace(/-/g, '')
    const timeNow = d.getHours()

    const data = this.getData(today)
    data.then((result) => {
      if (result) {
        this.setState({
          todoLists: result.todoLists,
          habitList: result.habitList,
          important: result.important,
          worthlessLimit: result.worthlessLimit,
          worthless: result.worthless
        })

        if (timeNow === 23) {
          _self.dailyInit()
          //push notification to check report
        }
      }
    })
  }

  dailyInit = () => {
    const checkLists = ['todoLists', 'important', 'habitList', 'worthlessLimit', 'worthless']
    let analizedLists = []
    let accuData
    this.getData('accuData').then((result) => {
      accuData = result.length !== 0 ? result : []
      
      for (item in checkLists) {
        const data = this.analizeData(this.state[checkLists[item]])
        analizedLists.push(data)
      }
      const accuDataset = {
        date : new Date().toLocaleDateString(),
        data : this.analizeData(this.state.todoLists)
      }
      accuData.push(accuDataset)
  
      this.storeData('analized_' + new Date().toLocaleDateString().replace(/-/g, ''), analizedLists)
      this.storeData('accuData', accuData)

      this.setState({
        todoLists: [...this.state.habitList],
        important: [],
        worthlessLimit: [],
        worthless: [],
      })
    })
  }

  analizeData = (data) => {
    
    const dataset = {
      totalLen : data ? data.length : 0,
      isDoneLen : _.filter(data, {isDone: 1}) ? _.filter(data, {isDone: 1}).length : 0,
      failedLen : _.filter(data, {isExpired: true}) ? _.filter(data, {isExpired: true}).length : 0
    }
    const notDoneLen = dataset.totalLen - dataset.isDoneLen
    dataset['notDoneLen'] = notDoneLen
    dataset['date'] = new Date().toLocaleDateString()

    return dataset
  }

  render() {
    const inputAccessoryViewID = "uniqeID"

    return (
      <View style={styles.container}>
        
        <View style={{justifyContent:'flex-start', flex:1}}>
          <TextInput
            style={styles.textinput}
            onChangeText={(text) => this.setState({inputText: text})}
            onSubmitEditing={this.keyPressed.bind(this)}
            onBlur={this.initInput.bind(this)}
            placeholder={this.state.text}
            autoCapitalize="none"
            inputAccessoryViewID={inputAccessoryViewID}
            autoCorrect={false}
            ref={input => { this.textInput = input }}
          />

          <TabView
            navigationState={this.state}
            renderScene={SceneMap({
              all: this._showAllTodo.bind(this),
              red: this._FirstRoute.bind(this),
              yellow: this._SecondRoute.bind(this),
              green: this._ThirdRoute.bind(this),
              blue: this._ForthRoute.bind(this)
            })}
            onIndexChange={index => this.setState({ index })}
            renderTabBar={this._renderTabBar}
            swipeEnabled={false} />

          <CalendarModal 
            toggleCalendar={this.state.toggleCalendar} 
            closeModalPr={this.closeModal} 
            updateDatePicker={this.pickCertainDate} />
          
          <TimePickerModal 
            isDateTimePickerVisible={this.state.toggleTimePicker}
            hideDateTimePicker={this.closeTimePicker}
            closeModalPr={this.closeModal}
            handleTimePicked={this.handleTimePicked} />

          <InputAccessories 
            openCalendar={this.openCalendar}
            openTimePicker={this.openTimePicker}
            updateState={this.setState.bind(this)}
            isSetDate={this.state.isSetDate}
            isSetRepeat={this.state.isSetRepeat}
            isSetTimer={this.state.isSetTimer} />
          
          <FloatingBtn 
            deleteAll={this.deleteAll.bind(this)}
            navigate={this.props.navigation.navigate} />

        </View>
      </View>
    )
  }
}

const styles = StyleSheet.create({
    container: {
      padding: 10,
      height: '100%',
      flex:1
    },
    textinput: {
      height: 35, 
      borderColor: 'rgba(154,154,154,0.5)', 
      borderWidth: 1, 
      padding:7, 
      color:'rgba(154,154,154,0.7)'
    },
    activebtn: {
      opacity: 1
    },
    unactivebtn: {
      opacity: 0.2
    }
  });