import React, { Component } from "react";
import RN, {
  View,
  StyleSheet,
  FlatList
} from "react-native";
import { PieChart } from "react-native-svg-charts";
import AsyncStorage from "@react-native-community/async-storage";
import _ from "lodash";
import { ScrollView } from "react-native-gesture-handler";
import { Text } from "react-native-svg";
import Icon from 'react-native-vector-icons/Feather';
import {AdMobBanner} from 'react-native-admob';

function ColorLuminance(hex, lum) {
  // validate hex string
  hex = String(hex).replace(/[^0-9a-f]/gi, "");
  if (hex.length < 6) {
    hex = hex[0] + hex[0] + hex[1] + hex[1] + hex[2] + hex[2];
  }
  lum = lum || 0;

  // convert to decimal and change luminosity
  var rgb = "#",
    c,
    i;
  for (i = 0; i < 3; i++) {
    c = parseInt(hex.substr(i * 2, 2), 16);
    c = Math.round(Math.min(Math.max(0, c + c * lum), 255)).toString(16);
    rgb += ("00" + c).substr(c.length);
  }

  return rgb;
}

const Labels = ({ slices, height, width }) => {
  return slices.map((slice, index) => {
    const { labelCentroid, pieCentroid, data } = slice;

    if (data.value == 0) {
      return false;
    }

    return (
      <Text
        key={index}
        x={pieCentroid[0]}
        y={pieCentroid[1]}
        fill={ColorLuminance(data.svg.fill, -0.45)}
        textAnchor={pieCentroid[0] > 0 ? "start" : "end"}
        alignmentBaseline={"middle"}
        fontSize={14}
      >
        {slice.data.title + " : " + slice.value}
      </Text>
    );
  });
};

const PieChartElement = (props) => {
  const colorParret = [
    ["#447DB3", "#FF9A94", "#7ABFFF", "#C5CC70"],
    ["#FFCD24", "#E8A720", "#FFA530", "#E87420"],
    ["#FF802B", "#E85927", "#FF4E37", "#E82738"],
    ["#78FFC9", "#6DE895", "#85FF8C", "#8FE86D"],
    ["#6E7FFF", "#648EE8", "#7ABEFF", "#65C4EB"]
  ];
  const titleSet = [
    "Total",
    "Important",
    "Habit",
    "Worthless But Limited",
    "Worthless"
  ];
  const statusSet = ["Success", "Failed", "Progressing"];

  return (
    <ScrollView style={{ flex: 0.5 }}>
          {this.state.chartData.map((chartObj, key) => {
            const data = _.values(chartObj).slice(0, 4);
            const emptyData = [0, 0, 0, 0];

            if (_.isEqual(data, emptyData)) {
              return;
            }

            const pieData = data
              .filter((value, index) => index > 0 && typeof value === 'number')
              .map((value, index) => ({
                value,
                title: statusSet[index],
                svg: {
                  fill: colorParret[key][index],
                  onPress: () => console.log("press", index)
                },
                arc: {
                  padAngle: 0.04,
                  outerRadius:
                    (90 + value * 5 <= 100 ? 90 + value * 5 : 100) + "%",
                  innerRadius: "50%"
                },
                key: `pie-${index}`
              }));

            return (
              <View key={key} style={styles.contentView}>
                <RN.Text style={styles.title}>{titleSet[key]}</RN.Text>
                <PieChart
                  style={{ height: 200 }}
                  data={pieData}
                >
                  <Labels />
                </PieChart>
              </View>
            );
          })}
        </ScrollView>
  )
}

const EmptyElement = props => {
  return (
    <View style={styles.emptyElement}>
      <RN.Text style={{fontSize: 20}}>No Data</RN.Text>
    </View>
  )
}

export default class AchivementPage extends Component {
  constructor(props) {
    super(props);

    this.state = {
      chartData: [],
      accuData: []
    };
  }

  getData = async key => {
    try {
      let value;
      if (typeof key === "object") {
        value = await AsyncStorage.multiGet(key);
      } else if (typeof key === "string") {
        value = await AsyncStorage.getItem(key);
      }

      if (value !== null) {
        return value;
      }
    } catch (e) {
      console.log(e);
    }
  };

  componentDidMount() {
    const _self = this;
    const yesterday = this.getYesterday();
    // const test = "201966"; // yesterday.toString()

    this.getData(["analized_" + yesterday, "accuData"]).then(result => {
      if (result[0][1] && result[1][1]) {
        const charData = JSON.parse(result[0][1]);
        let accuData = JSON.parse(result[1][1]);
        
        if (accuData.length > 100 ) {
          accuData = _.drop(accuData, accuData.length - 100)
        }
        
        _self.setState({
          chartData: charData,
          accuData: accuData
        });
      }
    });
  }

  getYesterday() {
    const date = new Date();
    return parseInt(date.toLocaleDateString().replace(/-/g, "")) - 1;
  }

  _renderItem({ item }) {
    getPercentageOfAchivement = (total, done) => {
      if (total !== 0 && done !== 0) {
        return done / total * 100
      }
      return '0'
    }

    return (
      <View style={{flex:1, flexDirection:'row', paddingTop: 5, paddingBottom: 5, borderBottomColor: '#f8f8f8a5', borderBottomWidth:1}}>
        <RN.Text style={{...styles.dataTableCell, fontSize: 12, color: '#c8c8c8'}}>{item.date.slice(5).replace('-', '. ')}</RN.Text>
        <RN.Text style={{...styles.dataTableCell,}}>{item.data.totalLen}</RN.Text>
        <RN.Text style={{...styles.dataTableCell,}}>{item.data.isDoneLen}</RN.Text>
        <RN.Text style={{...styles.dataTableCell,}}>{item.data.failedLen}</RN.Text>
        <RN.Text style={{...styles.dataTableCell,}}>{item.data.notDoneLen}</RN.Text>
        <RN.Text style={{...styles.dataTableCell, color: '#7ABFFF', fontWeight:'bold'}}>{getPercentageOfAchivement(item.data.totalLen, item.data.isDoneLen) + '%'}</RN.Text>
      </View>
    
    );
  }

  _listHeader() {
    return (
      <View style={{flex:1, flexDirection:'row', marginBottom: 10}}>
        <RN.Text style={{...styles.dataTableCell, fontSize: 12, color: '#c8c8c8'}}>
          <Icon name="calendar" size={20} backgroundColor={'transparent'}></Icon>
        </RN.Text>
        <RN.Text style={{...styles.dataTableCell,}}>
          <Icon name="package" size={20} backgroundColor={'transparent'}></Icon>
        </RN.Text>
        <RN.Text style={{...styles.dataTableCell,}}>
          <Icon name="heart" size={20} backgroundColor={'transparent'} color="red"></Icon>
        </RN.Text>
        <RN.Text style={{...styles.dataTableCell,}}>
          <Icon name="check-square" size={20} backgroundColor={'transparent'}></Icon>
        </RN.Text>
        <RN.Text style={{...styles.dataTableCell,}}>
          <Icon name="skip-forward" size={20} backgroundColor={'transparent'}></Icon>
        </RN.Text>
        <RN.Text style={{...styles.dataTableCell,}}>
          <Icon name="award" size={20} backgroundColor={'transparent'}></Icon>
        </RN.Text>
      </View>
    )
  }

  render() {
    return (
      <View style={{ flex: 1 }}>
        {this.state.chartData.length === 0 ? 
          <EmptyElement></EmptyElement> : 
          <PieChartElement></PieChartElement>
        }
        <View style={{alignItems:'center'}}>
            <AdMobBanner 
              adUnitID='ca-app-pub-5635953962161470/6791504099' 
              testDevices={[AdMobBanner.simulatorId]}
              adSize='banner'
              />
          </View>
        <View style={{...styles.contentView, flex: 0.5}}>
          {this.state.accuData.length === 0 ?
            <EmptyElement></EmptyElement> :
            <FlatList
              ListHeaderComponent={this._listHeader}
              data={this.state.accuData}
              extraData={this.state.accuData}
              renderItem={this._renderItem}
              keyExtractor={(item, index) => index + ""}
            />
          }
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  contentView: {
    margin: 10
  },
  title: {
    fontWeight: "bold"
  },
  dataTableCell: {
    flex: 0.25,
    justifyContent: 'center',
    alignItems:'center',
    textAlign:'center'
  },
  emptyElement: { 
    flex: 0.5, 
    justifyContent:'center', 
    alignItems:'center', 
  }
});
